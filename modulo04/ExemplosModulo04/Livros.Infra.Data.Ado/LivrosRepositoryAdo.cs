﻿using Livros.Domain.Entities;
using Livros.Domain.Interfaces.Repositories;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;

namespace Livros.Infra.Data.Ado
{
    public class LivrosRepositoryAdo : ILivrosRepository
    {
        private string _connectionString;

        public LivrosRepositoryAdo()
        {
            _connectionString = ConfigurationManager.AppSettings["connectionStringMysql"];
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(_connectionString);
        }

        public bool Delete(int id)
        {
            using MySqlConnection conn = GetConnection();

            try
            {
                string query = $"delete from books where id = {id}";

                using MySqlCommand cmd = new MySqlCommand(query, conn);
                conn.Open();
                var rows = cmd.ExecuteNonQuery();

                if (rows > 0)
                    return true;

                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                    conn.Close();
            }
        }

        public List<Book> GetAll()
        {
            List<Book> livros = new List<Book>();

            using MySqlConnection conn = GetConnection();

            try
            {
                MySqlCommand cmd = new MySqlCommand("select * from books", conn);

                conn.Open();

                using var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var livro = new Book
                    {
                        Id = reader.GetInt32(0),
                        Author = reader.GetValue(1).ToString(),
                        Country = reader.GetValue(2).ToString(),
                        ImageLink = reader.GetValue(3).ToString(),
                        Language = reader.GetValue(4).ToString(),
                        Link = reader.GetValue(5).ToString(),
                        Pages = reader.GetInt32(6),
                        Title = reader.GetValue(7).ToString(),
                        Year = reader.GetInt32(8)
                    };

                    livros.Add(livro);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                    conn.Close();
            }

            return livros;
        }

        public int Create(Book livro)
        {
            using MySqlConnection conn = GetConnection();

            try
            {
                StringBuilder query = new StringBuilder();
                query.Append("insert into books (author, country, imageLink, language, link, pages, title, year) values");
                query.Append(" (@author, @country, @imageLink, @language, @link, @pages, @title, @year)");

                using MySqlCommand cmd = new MySqlCommand(query.ToString(), conn);
                cmd.Parameters.Add("@author", MySqlDbType.VarChar, 50).Value = livro.Author;
                cmd.Parameters.Add("@country", MySqlDbType.VarChar, 50).Value = livro.Country;
                cmd.Parameters.Add("@imageLink", MySqlDbType.VarChar, 200).Value = livro.ImageLink;
                cmd.Parameters.Add("@language", MySqlDbType.VarChar, 30).Value = livro.Language;
                cmd.Parameters.Add("@link", MySqlDbType.VarChar, 200).Value = livro.Link;
                cmd.Parameters.Add("@pages", MySqlDbType.Int32).Value = livro.Pages;
                cmd.Parameters.Add("@title", MySqlDbType.VarChar, 100).Value = livro.Title;
                cmd.Parameters.Add("@year", MySqlDbType.Int32).Value = livro.Year;

                conn.Open();
                cmd.ExecuteNonQuery();

                cmd.CommandText = "select max(id) from books";
                return (int)cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                    conn.Close();
            }
        }

        public bool Update(Book livro, int id)
        {
            using MySqlConnection conn = GetConnection();

            try
            {
                StringBuilder query = new StringBuilder();
                query.Append("update books set author=@author, country=@country, imageLink=@imageLink,");
                query.Append(" language=@language, link=@link, pages=@pages, title=@title, year=@year");
                query.Append(" where id=@id");

                using MySqlCommand cmd = new MySqlCommand(query.ToString(), conn);
                cmd.Parameters.Add("@id", MySqlDbType.Int32).Value = livro.Id;
                cmd.Parameters.Add("@author", MySqlDbType.VarChar, 50).Value = livro.Author;
                cmd.Parameters.Add("@country", MySqlDbType.VarChar, 50).Value = livro.Country;
                cmd.Parameters.Add("@imageLink", MySqlDbType.VarChar, 200).Value = livro.ImageLink;
                cmd.Parameters.Add("@language", MySqlDbType.VarChar, 30).Value = livro.Language;
                cmd.Parameters.Add("@link", MySqlDbType.VarChar, 200).Value = livro.Link;
                cmd.Parameters.Add("@pages", MySqlDbType.Int32).Value = livro.Pages;
                cmd.Parameters.Add("@title", MySqlDbType.VarChar, 100).Value = livro.Title;
                cmd.Parameters.Add("@year", MySqlDbType.Int32).Value = livro.Year;

                conn.Open();
                var rows = cmd.ExecuteNonQuery();

                if (rows > 0)
                    return true;

                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                    conn.Close();
            }
        }

        public bool DeleteAll()
        {
            using MySqlConnection conn = GetConnection();

            try
            {
                string query = "delete from books where id >= 0";

                using MySqlCommand cmd = new MySqlCommand(query, conn);
                conn.Open();
                var rows = cmd.ExecuteNonQuery();

                if (rows >= 0)
                    return true;

                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                    conn.Close();
            }
        }

        public Book GetById(int id)
        {
            using MySqlConnection conn = GetConnection();

            try
            {
                MySqlCommand cmd = new MySqlCommand($"select id, author, country, imageLink, language, link, pages, title, year from books" +
                    $" where id={id}", conn);

                conn.Open();

                using var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    var livro = new Book
                    {
                        Id = reader.GetInt32(0),
                        Author = reader.GetValue(1).ToString(),
                        Country = reader.GetValue(2).ToString(),
                        ImageLink = reader.GetValue(3).ToString(),
                        Language = reader.GetValue(4).ToString(),
                        Link = reader.GetValue(5).ToString(),
                        Pages = reader.GetInt32(6),
                        Title = reader.GetValue(7).ToString(),
                        Year = reader.GetInt32(8)
                    };

                    return livro;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                    conn.Close();
            }
        }
    }
}
