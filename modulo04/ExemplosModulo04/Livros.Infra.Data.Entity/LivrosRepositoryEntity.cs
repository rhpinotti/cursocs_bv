﻿using Livros.Domain.Entities;
using Livros.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Livros.Infra.Data.Entity
{
    public class LivrosRepositoryEntity : ILivrosRepository, IDisposable
    {
        public LivrosContext LivrosContext { get; set; }

        public LivrosRepositoryEntity()
        {
            LivrosContext = new LivrosContext();
        }

        public int Create(Book livro)
        {
            try
            {
                LivrosContext.Books.Add(livro);
                LivrosContext.SaveChanges();

                return livro.Id;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var livro = GetById(id);
                LivrosContext.Books.Remove(livro);
                LivrosContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool DeleteAll()
        {
            try
            {
                LivrosContext.Books.RemoveRange(LivrosContext.Books);
                LivrosContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Book GetById(int id)
        {
            try
            {
                Book livro = LivrosContext.Books.Where(l => l.Id == id).FirstOrDefault();

                return livro;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Update(Book livro, int id)
        {
            try
            {
                LivrosContext.Books.Update(livro);
                LivrosContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Book> GetAll()
        {
            try
            {
                return LivrosContext.Books.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Dispose()
        {
            LivrosContext.Dispose();
        }
    }
}
