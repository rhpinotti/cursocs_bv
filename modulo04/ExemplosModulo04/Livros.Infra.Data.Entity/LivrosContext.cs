﻿using Livros.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Configuration;

namespace Livros.Infra.Data.Entity
{
    public class LivrosContext : DbContext
    {
        public DbSet<Book> Books { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseMySQL(ConfigurationManager.AppSettings["connectionStringMysql"]);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Book>(book =>
            {
                book.ToTable("books");
                book.HasKey(a => a.Id);
                book.Property(a => a.Author);
            });
        }
    }
}
