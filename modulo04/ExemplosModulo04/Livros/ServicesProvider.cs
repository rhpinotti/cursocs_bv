﻿using Livros.Domain.Interfaces.Repositories;
using Livros.Infra.Data.Ado;
using Livros.Infra.Data.Entity;
using Microsoft.Extensions.DependencyInjection;

namespace Livros
{
    public class ServicesProvider
    {
        public static ServiceProvider Provider { get; private set; }

        public void CreateProvider()
        {
            var serviceProvider = new ServiceCollection()
                //.AddSingleton<ILivrosRepository, LivrosRepositoryAdo>()
                .AddSingleton<ILivrosRepository, LivrosRepositoryEntity>()
                .BuildServiceProvider();

            Provider = serviceProvider;
        }
    }
}
