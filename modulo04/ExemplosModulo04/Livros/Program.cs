﻿using Livros.Domain.Entities;
using Livros.Domain.Interfaces.Repositories;
using System;

namespace Livros
{
    class Program
    {
        static ILivrosRepository livrosRepository;

        static void Main(string[] args)
        {
            bool continua = true;

            (new ServicesProvider()).CreateProvider();
            livrosRepository = ServicesProvider.Provider.GetService(typeof(ILivrosRepository)) as ILivrosRepository;

            //ObterLivros();

            while (continua)
            {
                Console.WriteLine("");
                Console.WriteLine($"1. Listar livros");
                Console.WriteLine($"2. Inserir livro");
                Console.WriteLine($"3. Excluir livros");
                Console.WriteLine($"4. Atualiza autor");
                Console.WriteLine($"5. Excluir livro");
                Console.WriteLine($"0. Encerrar");

                Console.Write("\nEscolha uma opção: ");
                char opcao = Console.ReadKey().KeyChar;

                switch (opcao)
                {
                    case '0':
                        continua = false;
                        break;
                    case '1':
                        ObterLivros();
                        break;
                    case '2':
                        InserirLivro();
                        ObterLivros();
                        break;
                    case '3':
                        ExcluirTodos();
                        ObterLivros();
                        break;
                    case '4':
                        AtualizarAutorLivro();
                        ObterLivros();
                        break;
                    case '5':
                        ExcluirLivro();
                        ObterLivros();
                        break;
                }

                Console.Clear();
            }
        }

        static void ObterLivros()
        {
            Console.WriteLine("...buscando livros");

            var lista = livrosRepository.GetAll();
            Console.Clear();

            foreach (var livro in lista)
            {
                Console.WriteLine($"\tId: {livro.Id}\n" +
                    $"\tAutor: {livro.Author}\n" +
                    $"\tTítulo: {livro.Title}\n");
            }

            Console.WriteLine($"\nTotal de livros: {lista.Count}");

            Console.ReadKey();
        }

        static void InserirLivro()
        {
            var numeroLivro = (new Random()).Next(1, 1000);

            var livro = new Book
            {
                Author = $"Autor {numeroLivro}",
                Title = $"Título teste {numeroLivro}"
            };

            livrosRepository.Create(livro);
        }

        static void ExcluirTodos()
        {
            livrosRepository.DeleteAll();
        }

        static void AtualizarAutorLivro()
        {
            try
            {
                Console.Write("Digite o id desejado: ");
                int id = int.Parse(Console.ReadLine());

                Console.Write("Digite o novo nome do autor: ");
                string autor = Console.ReadLine().Trim().ToLower();

                var livro = livrosRepository.GetById(id);

                if (livro != null)
                {
                    livro.Author = autor;
                    livrosRepository.Update(livro, id);
                }
                else
                {
                    Console.WriteLine($"Livro com id {id} não encontrado.");
                    Console.ReadKey();
                    Console.Clear();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Falha ao atualizar livro:\n{ex.Message}");
                Console.ReadKey();
                Console.Clear();
            }
        }

        static void ExcluirLivro()
        {
            try
            {
                Console.Write("Digite o id desejado: ");
                int id = int.Parse(Console.ReadLine());

                var livro = livrosRepository.GetById(id);

                if (livro != null)
                {
                    livrosRepository.Delete(id);
                }
                else
                {
                    Console.WriteLine($"Livro com id {id} não encontrado.");
                    Console.ReadKey();
                    Console.Clear();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Falha ao excluir livro:\n{ex.Message}");
                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}
