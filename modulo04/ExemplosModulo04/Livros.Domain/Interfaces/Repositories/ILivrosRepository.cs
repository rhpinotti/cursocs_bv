﻿using Livros.Domain.Entities;
using System.Collections.Generic;

namespace Livros.Domain.Interfaces.Repositories
{
    public interface ILivrosRepository
    {
        int Create(Book livro);
        bool Update(Book livro, int id);
        bool Delete(int id);
        Book GetById(int id);
        List<Book> GetAll();

        bool DeleteAll();
    }
}
