﻿using System;

namespace Exercicio05
{
    class Program
    {
        static void Main(string[] args)
        {
            Aluno aluno = new Aluno();

            Console.WriteLine("--- Exercício 05");
            Console.Write("Nome aluno normal: ");
            aluno.Nome = Console.ReadLine();

            Console.Write("Digite as notas (separadas por espaço): ");
            string[] notasStr = Console.ReadLine().Split(' ');

            for (int i = 0; i < notasStr.Length; i++)
            {
                aluno.Notas.Add(float.Parse(notasStr[i]));
            }

            Console.Write("\n");

            var mediaFinal = aluno.CalcularMedia();

            string situacao = "APROVADO";
            if (!aluno.VerificarAprovacao())
                situacao = "REPROVADO";

            Console.Write($"---Aluno normal\n" +
                $"Média: {mediaFinal}\n" +
                $"Nota mínima para aprovação: {PadroesSistema.MediaMinima}\n" +
                $"Situação: {situacao}");

            Console.ReadKey();

            CalcularAlunoEspecial();
        }

        static void CalcularAlunoEspecial()
        {
            AlunoEspecial alunoEspecial = new AlunoEspecial();

            Console.Clear();

            Console.WriteLine("--- Exercício 05");
            Console.Write("Nome aluno especial: ");
            alunoEspecial.Nome = Console.ReadLine();

            Console.Write("Notas geradas aleatoriamente:");

            for (int i = 0; i < 8; i++)
            {
                Random random = new Random();

                double nota = random.NextDouble() * 10;

                Console.Write($" {nota.ToString("F2")}");

                alunoEspecial.Notas.Add((float)nota);
            }

            Console.Write("\n");

            var mediaFinal = alunoEspecial.CalcularMedia();

            string situacao = "APROVADO";
            if (!alunoEspecial.VerificarAprovacao())
                situacao = "REPROVADO";

            Console.Write($"---Aluno especial\n" +
                $"Média: {mediaFinal}\n" +
                $"Nota mínima para aprovação: {PadroesSistema.MediaMinimaEspecial}\n" +
                $"Situação: {situacao}");

            Console.ReadKey();
        }
    }
}
