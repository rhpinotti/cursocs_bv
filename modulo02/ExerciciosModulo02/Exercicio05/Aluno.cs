﻿using System.Collections.Generic;

namespace Exercicio05
{
    public class Aluno : Pessoa
    {
        public List<float> Notas { get; set; }

        public Aluno()
        {
            Notas = new List<float>();
        }

        public float CalcularMedia()
        {
            float media;
            float total = 0;
            int i = 0;

            foreach (var nota in Notas)
            {
                total += nota;
                i++;
            }

            media = total / i;

            return media;
        }

        public virtual bool VerificarAprovacao()
        {
            if (CalcularMedia() >= PadroesSistema.MediaMinima)
                return true;

            return false;
        }
    }
}
