﻿namespace Exercicio05
{
    public class AlunoEspecial : Aluno
    {
        public override bool VerificarAprovacao()
        {
            if (CalcularMedia() >= PadroesSistema.MediaMinimaEspecial)
                return true;

            return false;
        }
    }
}
