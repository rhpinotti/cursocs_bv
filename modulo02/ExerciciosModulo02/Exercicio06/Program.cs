﻿using System;

namespace Exercicio06
{
    class Program
    {
        static void Main(string[] args)
        {
            int contadorAceleracao = 0;
            int contadorReducao = 0;
            int contadorMarchas = 0;

            IVeiculo carro = new Carro();
            Moto moto = new Moto();

            while (true)
            {
                if (contadorAceleracao < 10)
                {
                    AcelerarVeiculo(carro);
                    AcelerarVeiculo(moto);
                    
                    contadorAceleracao++;

                    Console.WriteLine("");
                }

                if (contadorMarchas < 5)
                {
                    TrocarMarcha(carro);
                    TrocarMarcha(moto);
                    
                    contadorMarchas++;

                    Console.WriteLine("");
                }

                if (contadorReducao < 6)
                {
                    DesacelerarVeiculo(carro);
                    DesacelerarVeiculo(moto);
                    
                    contadorReducao++;

                    Console.WriteLine("");
                }

                if (contadorAceleracao >= 10 && contadorReducao >= 6 && contadorMarchas >= 5)
                    break;
            }

            Console.ReadKey();
        }

        static void AcelerarVeiculo(IVeiculo veiculo)
        {
            Random random = new Random();
            var velocidade = random.Next(5, 100);

            velocidade = veiculo.Acelerar(velocidade);

            Console.Write($" + {veiculo.GetType().ToString().Split('.')[1]}: {velocidade} Km/h".PadRight(25));
        }

        static void DesacelerarVeiculo(IVeiculo veiculo)
        {
            Random random = new Random();
            var velocidade = random.Next(5, 100);
            velocidade = veiculo.Reduzir(velocidade);

            Console.Write($" - {veiculo.GetType().ToString().Split('.')[1]}: {velocidade} Km/h".PadRight(25));
        }

        static void TrocarMarcha(IVeiculo veiculo)
        {
            Random random = new Random();
            var marcha = random.Next(1, 5);
            marcha = veiculo.TrocarMarcha(marcha);

            Console.Write($" # {veiculo.GetType().ToString().Split('.')[1]}: {marcha}a marcha".PadRight(25));
        }
    }
}
