﻿namespace Exercicio06
{
    public interface IVeiculo
    {
        int TrocarMarcha(int marcha);
        int Acelerar(int velocidade);
        int Reduzir(int velocidade);
    }
}
