﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercicio06
{
    public class Carro : IVeiculo
    {
        public int Velocidade { get; private set; }
        public int Marcha { get; private set; }

        public Carro()
        {
            Velocidade = 0;
            Marcha = 0;
        }

        public int Acelerar(int velocidade)
        {
            Velocidade += velocidade;

            if (Velocidade >= 200)
                Velocidade = 200;

            return Velocidade;
        }

        public int Reduzir(int velocidade)
        {
            Velocidade -= velocidade;

            if (Velocidade < 0)
                Velocidade = 0;

            return Velocidade;
        }

        public int TrocarMarcha(int marcha)
        {
            return Marcha = marcha;
        }
    }
}
