﻿using System.Collections.Generic;

namespace Exercicio03
{
    public class Aluno
    {
        public string Nome { get; set; }
        public List<float> Notas { get; set; }

        public Aluno()
        {
            Notas = new List<float>();
        }

        public float CalcularMedia()
        {
            float media;
            float somaNotas = 0;
            int totalNotas = 0;

            foreach (var nota in Notas)
            {
                somaNotas += nota;
                totalNotas++;
            }

            media = somaNotas / totalNotas;

            return media;
        }
    }
}
