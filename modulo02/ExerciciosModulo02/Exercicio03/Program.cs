﻿using System;

namespace Exercicio03
{
    class Program
    {
        static void Main(string[] args)
        {
            Aluno aluno01 = new Aluno();

            Console.Write("Nome: ");
            aluno01.Nome = Console.ReadLine();

            Console.Write("Digite as notas (separadas por espaço): ");
            string[] notasStr = Console.ReadLine().Split(' ');

            for (int i = 0; i < notasStr.Length; i++)
            {
                aluno01.Notas.Add(float.Parse(notasStr[i]));
            }

            Console.Write("\n");

            string situacao = "APROVADO";

            float mediaFinal = aluno01.CalcularMedia();

            if (mediaFinal < ParametrosSistema.NotaMinima)
                situacao = "REPROVADO";


            Console.Write($"Média: {mediaFinal}\n" +
                $"Nota mínima para aprovação: {ParametrosSistema.NotaMinima}\n" +
                $"Situação: {situacao}");

            Console.ReadKey();
        }
    }
}
