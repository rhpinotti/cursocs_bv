﻿using System;

namespace Exercicio01
{
    class Program
    {
        static void Main(string[] args)
        {
            Pessoa pessoa1 = new Pessoa();
            Pessoa pessoa2 = new Pessoa();

            Console.Write("Digite o nome 1: ");
            pessoa1.Nome = Console.ReadLine();
            Console.Write("Digite a idade 1: ");
            pessoa1.Idade = int.Parse(Console.ReadLine());

            Console.Write("Digite o nome 2: ");
            pessoa2.Nome = Console.ReadLine();
            Console.Write("Digite a idade 2: ");
            pessoa2.Idade = int.Parse(Console.ReadLine());

            Console.Write($"\nPessoa mais velha: ");
            if (pessoa1.Idade == pessoa2.Idade)
                Console.WriteLine($"Idades iguais {pessoa2.Idade} anos");
            else if (pessoa1.Idade > pessoa2.Idade)
                Console.WriteLine($"{pessoa1.Nome}, idade {pessoa1.Idade} anos");
            else
                Console.WriteLine($"{pessoa2.Nome}, idade {pessoa2.Idade} anos");

            Console.ReadKey();
        }
    }
}
