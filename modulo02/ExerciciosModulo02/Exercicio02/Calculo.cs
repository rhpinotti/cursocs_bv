﻿namespace Exercicio02
{
    public class Calculo
    {
        public double TaxaImpostos { get; set; }

        public double CalcularSalarioLiquido(double salario)
        {
            return salario - (salario * TaxaImpostos / 100);
        }

        public double CalcularAumentoSalarial(double salario, double percentualAumento)
        {
            double salarioCalculado = salario + (salario * percentualAumento / 100);

            return salarioCalculado;
        }
    }
}
