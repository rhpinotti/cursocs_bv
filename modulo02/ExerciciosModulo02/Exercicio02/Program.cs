﻿using System;

namespace Exercicio02
{
    class Program
    {
        static void Main(string[] args)
        {
            Funcionario funcionario = new Funcionario();
            Calculo calculo = new Calculo();

            Item01(funcionario, calculo);
            Item02(funcionario, calculo);

            Console.ReadKey();
        }

        static void Item02(Funcionario funcionario, Calculo calculo)
        {
            Console.Write("Percentual de aumento (%): ");
            funcionario.PercentualAumento = double.Parse(Console.ReadLine());

            double salarioComAumento = calculo.CalcularAumentoSalarial(funcionario.SalarioBruto, funcionario.PercentualAumento);

            Console.WriteLine($"Salário líquido após aumento: R$ {calculo.CalcularSalarioLiquido(salarioComAumento)}");
        }

        static void Item01(Funcionario funcionario, Calculo calculo)
        {
            Console.Write("Nome: ");
            funcionario.Nome = Console.ReadLine();

            Console.Write("Salário bruto: ");
            funcionario.SalarioBruto = double.Parse(Console.ReadLine());

            Console.Write("Taxa imposto (%): ");
            calculo.TaxaImpostos = double.Parse(Console.ReadLine());

            double salarioLiquido = calculo.CalcularSalarioLiquido(funcionario.SalarioBruto);
            Console.WriteLine($"Salário líquido: R$ {salarioLiquido}");
        }
    }
}
