﻿namespace Exercicio04
{
    public class Calculo
    {
        public double TaxaImpostos { get; set; }

        public double CalcularSalarioLiquido(double salario)
        {
            return salario - (salario * TaxaImpostos / 100);
        }

        public double CalcularSalarioLiquido(double salario, double taxaImpostos)
        {
            return salario - (salario * taxaImpostos / 100);
        }

        public double CalcularAumentoSalarial(double salario, double percentualAumento)
        {
            double salarioCalculado = salario + (salario * percentualAumento / 100);

            return salarioCalculado;
        }
    }
}
