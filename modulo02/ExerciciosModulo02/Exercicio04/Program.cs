﻿using System;

namespace Exercicio04
{
    class Program
    {
        static void Main(string[] args)
        {
            Funcionario funcionario = new Funcionario();
            Calculo calculo = new Calculo();

            Console.Write("Nome: ");
            funcionario.Nome = Console.ReadLine();

            Console.Write("Salário bruto: ");
            funcionario.SalarioBruto = double.Parse(Console.ReadLine());

            Console.Write("Taxa imposto (%): ");
            var taxaImpostos = double.Parse(Console.ReadLine());

            double salarioLiquido = calculo.CalcularSalarioLiquido(funcionario.SalarioBruto, taxaImpostos);
            Console.WriteLine($"Salário líquido: R$ {salarioLiquido}");

            Console.ReadKey();
        }
    }
}
