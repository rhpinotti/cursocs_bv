﻿namespace Exercicio04
{
    public class Funcionario
    {
        public string Nome { get; set; }
        public double SalarioBruto { get; set; }
        public double PercentualAumento { get; set; }
    }
}
