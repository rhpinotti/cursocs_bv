﻿using ArquivoTexto.Services;
using System;
using System.IO;

namespace ArquivoTexto
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var continua = true;

                while (continua)
                {
                    Console.Clear();
                    Console.WriteLine($"Escolha uma opção:");
                    Console.WriteLine($"\t1 - Arquivo texto");
                    Console.WriteLine($"\t2 - Arquivo xml");
                    Console.WriteLine($"\t3 - Arquivo json");
                    Console.WriteLine($"\t4 - Arquivo csv");
                    Console.WriteLine($"\tF - Encerrar");
                    var opcao = Console.ReadKey().KeyChar;

                    Console.Clear();

                    switch (opcao)
                    {
                        case '1':
                            ArquivoTexto();
                            Console.ReadKey();
                            break;
                        case '2':
                            ArquivoXml();
                            Console.ReadKey();
                            break;
                        case '3':
                            ArquivoJson();
                            Console.ReadKey();
                            break;
                        case '4':
                            ArquivoCsv();
                            Console.ReadKey();
                            break;
                        case 'f':
                            continua = false;
                            break;
                        default:
                            Console.WriteLine($"\nOpção {opcao} inválida!!!\nPressione qualquer tecla para continuar.");
                            Console.ReadKey();
                            break;
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Fala geral!!!!\nPressione qualquer tecla para encerrar.");
                Console.ReadKey();
            }
        }

        static void ArquivoTexto()
        {
            string pathArquivo = Directory.GetCurrentDirectory() + @"\Files\arquivo.txt";

            var arquivoTextoService = new ArquivoTextoService(pathArquivo);
            arquivoTextoService.LerArquivoFile();
            Console.WriteLine($"* Arquivo original:\n{arquivoTextoService.BuscarLinhasArquivo()}");

            Console.Write($"Frase a ser adicionada ao arquivo: ");
            var frase = Console.ReadLine();
            arquivoTextoService.IncluirLinhaArquivo(frase);
            arquivoTextoService.LerArquivoFile();
            Console.WriteLine($"\n* Arquivo alterado:\n{arquivoTextoService.BuscarLinhasArquivo()}");
        }

        static void ArquivoXml()
        {
            try
            {
                string pathArquivo = Directory.GetCurrentDirectory() + @"\Files\books.xml";

                var service = new ArquivoXmlService(pathArquivo);
                var livros = service.ObterLivrosXml();

                foreach (var livro in livros)
                {
                    Console.WriteLine($"Id: {livro.Id}\n" +
                        $"Autor: {livro.Author}\n" +
                        $"Título: {livro.Title}\n" +
                        $"Gênero: {livro.Genre}\n" +
                        $"Preço: {livro.Price.ToString("C2")}\n" +
                        $"Data de publicação: {livro.PublishDate}\n" +
                        $"Descrição: {livro.Description}\n");
                }

                Console.WriteLine($"Total de livros: {livros.Count}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        static void ArquivoJson()
        {
            try
            {
                string pathArquivo = Directory.GetCurrentDirectory() + @"\Files\books.json";

                var service = new ArquivoJsonService(pathArquivo);
                var livros = service.ObterLivrosJson();

                foreach (var livro in livros)
                {
                    Console.WriteLine($"Autor: {livro.Author}\n" +
                        $"País: {livro.Country}\n" +
                        $"Imagem: {livro.ImageLink}\n" +
                        $"Link: {livro.Link}" +
                        $"Idioma: {livro.Language}\n" +
                        $"Páginas: {livro.Pages}\n" +
                        $"Título: {livro.Title}\n" +
                        $"Ano: {livro.Year}\n");
                }

                Console.WriteLine($"Total de livros: {livros.Count}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        static void ArquivoCsv()
        {
            try
            {
                string pathArquivo = Directory.GetCurrentDirectory() + @"\Files\books.csv";

                var service = new ArquivoCsvService(pathArquivo);
                var livros = service.ObterLivrosCsv();

                foreach (var livro in livros)
                {
                    Console.WriteLine($"Autor: {livro.Author}\n" +
                        $"País: {livro.Country}\n" +
                        $"Imagem: {livro.ImageLink}\n" +
                        $"Link: {livro.Link}" +
                        $"Idioma: {livro.Language}\n" +
                        $"Páginas: {livro.Pages}\n" +
                        $"Título: {livro.Title}\n" +
                        $"Ano: {livro.Year}\n");
                }

                Console.WriteLine($"Total de livros: {livros.Count}");
            }
            catch (DomainException dex)
            {
                Console.WriteLine($"DomainException {dex.Message}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
