﻿using Newtonsoft.Json;

namespace ArquivoTexto.Entities
{
    public class BookJsonCsv
    {
        [JsonProperty("author")]
        public string Author { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("imageLink")]
        public string ImageLink { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("pages")]
        public int Pages { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("year")]
        public int Year { get; set; }
    }
}