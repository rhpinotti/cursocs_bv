﻿using System;

namespace ArquivoTexto.Entities
{
    public class BookXml
    {
        public string Id { get; private set; }
        public string Author { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public double Price { get; set; }
        public DateTime PublishDate { get; set; }
        public string Description { get; set; }

        public BookXml(string id)
        {
            Id = id;
        }
    }
}