﻿using ArquivoTexto.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ArquivoTexto.Services
{
    public class ArquivoCsvService
    {
        private string _pathArquivo;

        public ArquivoCsvService(string pathArquivo)
        {
            _pathArquivo = pathArquivo;
        }

        public List<BookJsonCsv> ObterLivrosCsv()
        {
            var listaLivros = new List<BookJsonCsv>();

            FileStream fileStream = null;
            StreamReader reader = null;

            int i = 0;

            try
            {
                fileStream = new FileStream(_pathArquivo, FileMode.Open);
                reader = new StreamReader(fileStream);

                //para ler a primeira linha de cabeçalho
                reader.ReadLine();

                while (!reader.EndOfStream)
                {
                    i++;
                    
                    string[] linha = reader.ReadLine().Split(';');
                    listaLivros.Add(new BookJsonCsv()
                    {
                        Author = linha[0],
                        Country = linha[1],
                        ImageLink = linha[2],
                        Language = linha[3],
                        Link = linha[4],
                        Pages = int.Parse(linha[5]),
                        Title = linha[6],
                        Year = int.Parse(linha[7])
                    });
                }
            }
            catch
            {
                throw new DomainException($"Linha {i}");
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                if (fileStream != null)
                    fileStream.Close();
            }

            return listaLivros;
        }
    }
}
