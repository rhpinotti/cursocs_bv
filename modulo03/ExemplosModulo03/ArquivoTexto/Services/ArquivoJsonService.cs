﻿using ArquivoTexto.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace ArquivoTexto.Services
{
    public class ArquivoJsonService
    {
        private string _pathArquivo;

        public ArquivoJsonService(string pathArquivo)
        {
            _pathArquivo = pathArquivo;
        }

        public List<BookJsonCsv> ObterLivrosJson()
        {
            var textoJson = File.ReadAllText(_pathArquivo);

            var listaLivros = JsonConvert.DeserializeObject<List<BookJsonCsv>>(textoJson);

            return listaLivros;
        }
    }
}
