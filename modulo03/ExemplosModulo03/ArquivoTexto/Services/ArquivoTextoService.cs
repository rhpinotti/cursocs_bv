﻿using System.IO;
using System.Text;

namespace ArquivoTexto.Services
{
    public class ArquivoTextoService
    {
        private string _pathArquivo;
        private StringBuilder _linhasArquivo;

        public ArquivoTextoService(string pathArquivo)
        {
            _pathArquivo = pathArquivo;
        }

        public void LerArquivoFile()
        {
            _linhasArquivo = new StringBuilder();

            using StreamReader reader = File.OpenText(_pathArquivo);
            while (!reader.EndOfStream)
            {
                _linhasArquivo.Append(reader.ReadLine() + "\n");
            }
        }

        public string BuscarLinhasArquivo()
        {
            return _linhasArquivo.ToString();
        }

        public void IncluirLinhaArquivo(string linha)
        {
            using StreamWriter writer = File.AppendText(_pathArquivo);
            writer.Write($"\n{linha}");
        }
    }
}
