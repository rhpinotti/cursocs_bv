﻿using ArquivoTexto.Entities;
using System;
using System.Collections.Generic;
using System.Xml;

namespace ArquivoTexto.Services
{
    public class ArquivoXmlService
    {
        private string _pathArquivo;

        public ArquivoXmlService(string pathArquivo)
        {
            _pathArquivo = pathArquivo;
        }

        public List<BookXml> ObterLivrosXml()
        {
            var listaLivros = new List<BookXml>();

            BookXml livro = null;

            string elemento = "";

            using XmlTextReader reader = new XmlTextReader(_pathArquivo);
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if (reader.Name == "book")
                            livro = new BookXml(reader.GetAttribute(0));
                        elemento = reader.Name;
                        break;
                    case XmlNodeType.Text:
                        if (livro != null)
                            PopulaPropriedadesLivro(elemento, reader.Value, livro);
                        break;
                    case XmlNodeType.EndElement:
                        if (reader.Name == "book")
                        {
                            if (livro != null)
                                listaLivros.Add(livro);

                            livro = null;
                        }
                        break;
                }
            }

            return listaLivros;
        }

        private void PopulaPropriedadesLivro(string propriedade, string valor, BookXml livro)
        {
            switch (propriedade)
            {
                case "author":
                    livro.Author = valor;
                    break;
                case "title":
                    livro.Title = valor;
                    break;
                case "genre":
                    livro.Genre = valor;
                    break;
                case "price":
                    livro.Price = double.Parse(valor);
                    break;
                case "publish_date":
                    livro.PublishDate = DateTime.Parse(valor);
                    break;
                case "description":
                    livro.Description = valor;
                    break;
            }
        }
    }
}
