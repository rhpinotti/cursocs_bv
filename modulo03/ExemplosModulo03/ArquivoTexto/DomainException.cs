﻿using System;

namespace ArquivoTexto
{
    public class DomainException : Exception
    {
        public DomainException(string message) : base(message) { }
    }
}
